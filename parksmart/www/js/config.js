angular.module('Parksmart.config', [])
.constant('DB_CONFIG', {
    name: 'ParkSmartDB',
    tables: [
      {
            name: 'vehiclesParked',
            columns: [
                {name: 'licence_number', type: 'varchar primary key'},
                {name: 'permit_number', type: 'varchar'},
                {name: 'is_parked', type: 'bool'},
                {name: 'parked_date', type: 'varchar'},
                {name: 'property_id',type:'int'},
                {name: 'zone_id',type:'int'},
                {name: 'tenant_id',type:'int'},
                {name: 'tenant_name',type:'varchar'},
                {name: 'tenant_permit_allowed',type:'int'},
                {name: 'parking_allowed_zone', 'type':'varchar'},
                {name: 'is_synced', type: 'bool'},
            ]
       },
       {
       		name: 'parkingZones',
            columns: [
                {name: 'zone_id', type: 'int primary key'},
                {name: 'zone_name', type: 'varchar'},
                {name : 'property_id', type:'int'}
            ]
       },
       {
       		name: 'properties',
            columns: [
                {name: 'property_id', type: 'int primary key'},
                {name: 'property_name', type: 'varchar'},
                
            ]
       },
       {
       		name : 'tenant_zone',
       		columns: [
       		{name : 'tenant_id',type:'int'},
       		{name : 'zone_id',type:'int'},
       		{name : 'reserved',type :'int'}
       		],
       		primary: [
       		{name : 'tenant_id'},
       		{name : 'zone_id'}
       		]
       }
    ]
})
.constant('URL',{
	base_value : 'http://parksmart.intelliswift.co.in/services/' //'http://172.20.1.222:3000/services/' //'http://parksmart.intelliswift.co.in/services/' //'http://172.20.1.222:3000/services/' //'http://private-ee523-parksmart.apiary-mock.com/' 
})
.constant('MESSAGE',{
	connection_error : 'Connection problem. Please check your network connectivity',
	vehicle_already_parked : 'Employee vehicle is already parked',
	not_valid_parking : "This car is not valid to park in this parking zone",
	no_property_found : 'No property found for this user',
	no_parking_zone_found : 'No parking zone for this property added',
	vehicle_added : 'Verified vehicle added successfully',
	vehicle_removed : 'Removed vehicle from verified list',
	invalid_parking : "Invalid parking",
	offline_downloaded : "Offline data downloaded succeessfully",
	tenant_parking_full :"Invalid parking, tenant parking is full",
	parking_zone_full :"Invalid parking, tenant parking zone is full"
})
.constant('SECURITY',{
	salt : 'Intelli$123'
});