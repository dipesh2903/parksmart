var auth_token;
var user_id;
var companyName;

angular.module('Parksmart.controllers', ['Parksmart.services', 'Parksmart.config']).controller('AppCtrl', function($rootScope, $scope, $window, $ionicModal, $ionicPopup, $timeout, $location, $ionicLoading, Network, onlineStatus, MESSAGE, VehiclesParked, $ionicScrollDelegate, $ionicHistory, $ionicPlatform) {

	var today = new Date();

	function getMonthOrDay(currentValue) {
		if (currentValue < 10) {
			currentValue = '0' + currentValue;
		}
		return currentValue;
	}

	function parse(type) {
		return typeof type == 'string' ? JSON.parse(type) : type;
	}

	function CheckInternet() {
		var myNetwork = navigator.network;
		if (myNetwork == null || myNetwork == undefined) {
			console.log('NetStatus: ' + navigator.onLine);
			netStatus = navigator.onLine ? true : false;
			return netStatus;
		} else {
			console.log('Network: ' + myNetwork.connection.type);
			if (myNetwork.connection.type == Connection.NONE)
				return false;
			else
				return true;
		}
	}


	console.log(localStorage.getItem("isOnline"));
	if (localStorage.getItem("isOnline") == undefined || localStorage.getItem("isOnline") == null) {
		localStorage.setItem('isOnline', true);
	}

	$scope.date = "" + getMonthOrDay(today.getMonth() + 1) + "/" + getMonthOrDay(today.getDate()) + "/" + today.getFullYear();

	$scope.radioBtnSelection = true;

	var selectedProperty;

	var offlineSelectedProperty;

	var selectedParkingZone;

	var isPermitSelected = false;

	$scope.user = {};

	$scope.vehicles = [];
	$scope.vehicle = null;

	$scope.showSelected = false;

	$rootScope.notifyIonicGoingBack = function() {
		if ($location.path() == '/app/sync') {
			$scope.resetScreen();
		}

	};

	$scope.updateUserInfo = function() {
		if (localStorage.getItem("user") != undefined && localStorage.getItem("user") != "") {
			var user = JSON.parse(localStorage.getItem("user"));
			console.log(user);
			user_id = user.id;
			auth_token = user.auth_token;
			companyName = user.company;
			console.log(user_id + " " + auth_token);
			$scope.companyName = companyName;
		}

	};
	$scope.showLoading = function() {
		$ionicLoading.show({
			content : 'Loading',
			animation : 'fade-in',
			showBackdrop : true,
			maxWidth : 200,
			showDelay : 0
		});
	};

	$scope.hideLoading = function() {
		$ionicLoading.hide();
	};

	$scope.showConfirmOffline = function(message) {
		var confirmPopup = $ionicPopup.confirm({
			title : 'Parksmart',
			template : "To work on offline mode you need to download data, Do you wish to proceed now?"
		});
		confirmPopup.then(function(res) {
			if (res) {
				$scope.resetScreen();
				console.log('Yes');
				localStorage.setItem('isOnline', false);
				$scope.downloadOffline();

			} else {
				console.log('No');
			}
		});
	};

	$scope.changeMode = function() {
		if (CheckInternet()) {
			console.log('Push Notification Change', $scope.onlineMode.checked);
			if ($scope.onlineMode.checked) {
				localStorage.setItem('isOnline', true);
				$scope.pushOfflineData();
			} else {
				$scope.showConfirmOffline();
			}
		} else {
			$scope.showAlert(MESSAGE.connection_error);
			$scope.onlineMode = {
				checked : false,
			};

		}
	};

	console.log(localStorage.getItem('isOnline'));
	$scope.onlineMode = {
		checked : parse(localStorage.getItem('isOnline')),
	};

	$scope.getPropertyList = function() {

		$scope.showLoading();
		var data = {
			"userId" : user_id,
			"auth_token" : auth_token
		};
		console.log(data);
		if (parse(localStorage.getItem('isOnline'))) {

			if (CheckInternet()) {
				console.log(onlineStatus.isOnline());
				var request = Network.post(data, 'propertyList.json');

			} else {
				$ionicLoading.hide();
				$scope.reloadProperty(MESSAGE.connection_error);
			}

			request.success(function(response) {

				if (response.error_code == 0) {

					$scope.property = [];
					console.log(response);
					propertyList = response.data;

					angular.forEach(propertyList, function(value, key) {
						console.log(value.propertyName);
						$scope.property.push({
							id : value.id,
							label : value.propertyName
						});
						var property = {};
						property.propertyId = value.id;
						property.propertyName = value.propertyName;

						VehiclesParked.insertProperties(property).then(function(vehicles) {
							console.log(vehicles);
						});
						console.log($scope.property);
					});

				} else if (response.error_code == 3) {

					$scope.showAlert(response.message);
					localStorage.removeItem('user');
					location.href = '#/login';
				} else if (response.error_code == 4) {

					$scope.showAlert(MESSAGE.no_property_found);
				} else if (response.error_code != 0 && response.error_code != 3 && response.error_code != 4) {

					$scope.showAlert(response.message);
				}
				$scope.hideLoading();
			});
			request.error(function(response) {

			});
		} else {
			VehiclesParked.getProperties().then(function(response) {
				$ionicLoading.hide();
				console.log(response);
				$scope.property = [];

				propertyList = response;
				console.log(propertyList);
				angular.forEach(propertyList, function(value, key) {
					console.log(value.property_name);
					$scope.property.push({
						id : value.property_id,
						label : value.property_name
					});

				});
			});
		}
	};

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

		console.log("State changed: ", toState);

		if ($location.path() == "/app/parksmartInfo" || $location.path() == "/app/sync") {
			$scope.resetScreen();
			$ionicPlatform.ready(function() {
				$scope.getPropertyList();
			});
			$scope.updateUserInfo();
		}

	});

	$scope.getParkingZone = function(propertyId) {
		$scope.showLoading();
		var data = {
			"userId" : user_id,
			"auth_token" : auth_token,
			"propertyId" : propertyId
		};
		console.log(data);
		if (parse(localStorage.getItem('isOnline'))) {
			if (CheckInternet()) {
				console.log(CheckInternet());
				var request = Network.post(data, 'parkingZoneList.json');
			} else {
				$ionicLoading.hide();
				$scope.showAlert(MESSAGE.connection_error);
			}

			request.success(function(response) {
				if (response.error_code === 0) {
					$scope.showSelected = false;

					// if (angular.isDefined($scope.parkingZoneSelected)) {
					// alert($scope.parkingZoneSelected);
					// delete $scope.parkingZoneSelected;
					// }
					// selectedParkingZone = "";

					console.log(response);
					$scope.parkingZone = [];
					parkingZoneList = response.data;
					angular.forEach(parkingZoneList, function(value, key) {
						console.log("Name: " + value.zoneName);
						console.log("Id: " + value.id);
						$scope.parkingZone.push({
							id : value.id,
							label : value.zoneName
						});

						var parkingZone = {};
						parkingZone.zoneId = value.id;
						parkingZone.zoneName = value.zoneName;
						parkingZone.propertyId = selectedProperty;

						VehiclesParked.insertParkingZone(parkingZone).then(function(parkingZones) {
							console.log("ParkingZones added");
						});

						console.log("Parking Zone : " + $scope.parkingZone);
					});
					// if ($scope.parkingZone.length == 1) {
					//
					// $scope.showSelected = true;
					// $scope.Selected = $scope.parkingZone[0].label;
					// selectedParkingZone = $scope.parkingZone[0].id;
					// }
				} else if (response.error_code == 3) {

					$scope.showAlert(response.message);
					localStorage.removeItem('user');
					location.href = '#/login';
				} else if (response.error_code == 4) {

					$scope.showAlert(MESSAGE.no_parking_zone_found);
				} else if (response.error_code != 0 && response.error_code != 3 && response.error_code != 4) {

					$scope.showAlert(response.message);
				}
				$scope.hideLoading();
			});

			request.error(function(response) {

			});
		} else {
			VehiclesParked.getParkingZone(selectedProperty).then(function(response) {
				$ionicLoading.hide();
				console.log(response);

				// $ionicHistory.clearCache();
				// $scope.parkingZone = "";
				// selectedParkingZone = "";

				$scope.parkingZone = [];
				parkingZoneList = response;
				angular.forEach(parkingZoneList, function(value, key) {

					$scope.parkingZone.push({
						id : value.zone_id,
						label : value.zone_name
					});
					console.log("Parking Zone : " + $scope.parkingZone);
				});
				// if ($scope.parkingZone.length == 1) {
				//
				// $scope.showSelected = true;
				// $scope.Selected = $scope.parkingZone[0].label;
				// selectedParkingZone = $scope.parkingZone[0].id;
				// }
			});
		}
	};

	$scope.onPropertyChange = function(propertyId) {
		console.log(propertyId);
		$scope.resetResult();
		//alert(property.label);
		selectedProperty = propertyId;
		if (selectedProperty != null) {
			$ionicPlatform.ready(function() {
				$scope.getParkingZone(selectedProperty);
			});
		}
	};
	$scope.onOfflinePropertyChange = function(propertyId) {
		console.log(propertyId);
		//alert(property.label);
		offlineSelectedProperty = propertyId;

	};
	$scope.onParkingZoneChange = function(parkingZone) {
		console.log(parkingZone);
		//console.log($scope.parkingZoneSelected);

		selectedParkingZone = parkingZone;
		$scope.resetResult();

	};
	$scope.showConfirmLogout = function(message) {
		var confirmPopup = $ionicPopup.confirm({
			title : 'Parksmart',
			template : "Are you sure you want to logout?"
		});
		confirmPopup.then(function(res) {
			if (res) {
				$scope.resetScreen();
				console.log('Yes');
				VehiclesParked.dropTables();
				localStorage.removeItem('user');
				localStorage.removeItem('isOnline');

				location.href = '#/login';

			} else {
				console.log('No');
			}
		});
	};

	$scope.showAlert = function(message) {
		var alertPopup = $ionicPopup.alert({
			title : 'Parksmart',
			template : message
		});
		alertPopup.then(function(res) {
			/*
			 * Any thing which need to be performed after clicking OK button on alert
			 */
		});
	};

	$scope.reloadProperty = function(message) {
		var alertPopup = $ionicPopup.alert({
			title : 'Parksmart',
			template : message,
			okText : 'Try again'
		});
		alertPopup.then(function(res) {
			$scope.getPropertyList();
		});
	};

	$scope.logout = function() {

		$scope.showConfirmLogout();

	};

	$scope.getInfo = function(user) {
		if (user.permitNum != undefined && user.permitNum != "") {
			if (selectedProperty == undefined || selectedProperty == null || selectedProperty == "") {
				$scope.showAlert("Please select property");
			} else if (selectedParkingZone == undefined || selectedParkingZone == null || selectedParkingZone == "") {
				$scope.showAlert("Please select parking zone");
			} else {
				var data = {
					"userId" : user_id,
					"auth_token" : auth_token,
					"propertyId" : selectedProperty,
					"zoneId" : selectedParkingZone,
					"permitNumber" : angular.uppercase(user.permitNum),
					"date" : $scope.date
				};
				console.log(data);

				$scope.showLoading();
				if (parse(localStorage.getItem('isOnline'))) {
					if (CheckInternet()) {
						var request = Network.post(data, 'getInfoByPermit.json');

					} else {
						$scope.hideLoading();

						$scope.showAlert(MESSAGE.connection_error);
					}
				} else {
					$scope.hideLoading();
					VehiclesParked.getByPermit(angular.uppercase(user.permitNum), selectedProperty, $scope.date).then(function(response) {
						console.log(response);

						function addRecords(object) {
							records.push({
								isParked : parse(object.is_parked),
								licenceNumber : object.licence_number,
								permitNumber : object.permit_number,
								allowedParkingZones : object.parking_allowed_zone,
								tenantId : object.tenant_id,
								tenantName : object.tenant_Name,
								tenantPermitAllowed : object.tenant_permit_allowed
							});

						}

						var records = [];

						if (response.length > 0) {
							$scope.showHeader = 'true';
							var isAnyVehiceParked = false;
							angular.forEach(response, function(value, key) {
								addRecords(value);

								if (value.is_parked === 'true') {
									isAnyVehiceParked = true;

								}
							});

							if (isAnyVehiceParked) {
								$scope.showAlert(MESSAGE.vehicle_already_parked);
							}
							$scope.records = records;
							$ionicScrollDelegate.scrollBottom();
						} else {
							$scope.showAlert(MESSAGE.not_valid_parking);
						}

					});
				}
			}

		} else {
			if (selectedProperty == undefined || selectedProperty == null || selectedProperty == "") {
				$scope.showAlert("Please select property");
			} else if (selectedParkingZone == undefined || selectedParkingZone == null || selectedParkingZone == "") {
				$scope.showAlert("Please select parking zone");
			} else {
				var data = {
					"userId" : user_id,
					"auth_token" : auth_token,
					"propertyId" : selectedProperty,
					"zoneId" : selectedParkingZone,
					"licenceNumber" : angular.uppercase(user.licenceNum),
					"date" : $scope.date
				};
				console.log(data);
				if (parse(localStorage.getItem('isOnline'))) {
					if (CheckInternet()) {
						var request = Network.post(data, 'getInfoByLicence.json');

					} else {
						$ionicLoading.hide();
						$scope.showAlert(MESSAGE.connection_error);
					}
				} else {
					VehiclesParked.getByLicence(angular.uppercase(user.licenceNum), selectedProperty, $scope.date).then(function(response) {
						console.log(response);
						$scope.hideLoading();
						function addRecords(object) {
							records.push({
								isParked : parse(object.is_parked),
								licenceNumber : object.licence_number,
								permitNumber : object.permit_number,
								allowedParkingZones : object.parking_allowed_zone,
								tenantId : object.tenant_id,
								tenantName : object.tenant_name,
								tenantPermitAllowed : object.tenant_permit_allowed
							});

						}

						var records = [];

						if (response.length > 0) {
							$scope.showHeader = 'true';
							var isAnyVehiceParked = false;
							angular.forEach(response, function(value, key) {
								addRecords(value);

								if (value.is_parked === 'true') {
									isAnyVehiceParked = true;

								}
							});

							if (isAnyVehiceParked) {
								$scope.showAlert(MESSAGE.vehicle_already_parked);
							}
							$scope.records = records;
						} else {
							$scope.showAlert(MESSAGE.not_valid_parking);
						}

					});

				}
			}
		}
		request.success(function(response) {
			$scope.resetResult();
			$scope.hideLoading();
			console.log(response);
			if (response.error_code === 0) {
				function addRecords(object) {
					records.push({
						isParked : object.isParked,
						licenceNumber : object.licenceNumber,
						permitNumber : object.permitNumber,
						allowedParkingZones : object.allowedParkingZones,
						tenantId : object.tenantInfo[0].id,
						tenantName : object.tenantInfo[0].tenantName,
						tenantPermitAllowed : object.tenantInfo[0].numberOfPermits
					});
				}

				var records = [];

				if (response.data.length > 0) {
					$scope.showHeader = 'true';
					var isAnyVehiceParked = false;
					angular.forEach(response.data, function(value, key) {
						addRecords(value);
						var vehicleInfo = {};
						vehicleInfo.propertyId = selectedProperty;
						vehicleInfo.zoneId = selectedParkingZone;
						vehicleInfo.licenceNumber = value.licenceNumber;
						vehicleInfo.permitNumber = value.permitNumber;
						vehicleInfo.isParked = value.isParked;
						vehicleInfo.parkedDate = $scope.date;
						vehicleInfo.parkingAllowedZone = value.allowedParkingZones;
						vehicleInfo.isSynced = 'true';
						vehicleInfo.tenantId = value.tenantInfo[0].id;
						vehicleInfo.tenantName = value.tenantInfo[0].tenantName;
						vehicleInfo.tenantPermitAllowed = value.tenantInfo[0].numberOfPermits;

						var tenantParkingReserved = value.tenantInfo[0].parkingReserved;

						angular.forEach(tenantParkingReserved, function(data, key) {
							var tenantZone = {};
							tenantZone.tenantId = value.tenantInfo[0].id;
							tenantZone.zoneId = data.tenantZoneId;
							tenantZone.reserved = data.tenantParkingReserved;

							var tenantZones = [];
							VehiclesParked.getTenantZoneCount(tenantZone).then(function(tenantZones) {
								console.log("tenantZones : " + tenantZones.count);

								if (tenantZones.count <= 0) {

									VehiclesParked.insertTenantZone(tenantZone).then(function(result) {
										console.log(result);
									});

								} else {
									VehiclesParked.updateTenantZone(tenantZone).then(function(result) {
										console.log(result);
									});
								}
							});

						});

						VehiclesParked.getByLicence(vehicleInfo.licenceNumber, selectedProperty, $scope.date).then(function(vehicles) {
							console.log("Vehicle in DB : " + vehicles.length);
							console.log(vehicles);

							if (vehicles.length <= 0) {
								console.log("Insert into DB");
								VehiclesParked.insert(vehicleInfo).then(function(result) {
									console.log("Result of insert into DB");
									console.log(result);
								});
							} else {
								console.log("Update the DB : vehicle parked");
								VehiclesParked.update(vehicleInfo.licenceNumber, vehicleInfo.zoneId, vehicleInfo.isParked, $scope.date, true).then(function(vehicles) {
									console.log("Vehicles Updated : " + vehicles.length);
								});
							}
						});
						if (value.isParked) {
							isAnyVehiceParked = true;

						}

					});
					if (isAnyVehiceParked) {
						$scope.showAlert(MESSAGE.vehicle_already_parked);
					}
					$scope.records = records;
					$ionicScrollDelegate.scrollBottom();
				} else {
					$scope.showAlert(MESSAGE.not_valid_parking);
				}
			} else if (response.error_code == 3) {

				$scope.showAlert(response.message);
				localStorage.removeItem('user');
				location.href = '#/login';
			} else if (response.error_code === 4) {
				$scope.showAlert(MESSAGE.not_valid_parking);
			} else {
				$scope.showAlert(response.message);
			}
		});
		request.error(function(response) {
			$scope.hideLoading();
			console.log(response);
		});

	};

	$scope.vehicleParked = function(object) {
		console.log(object);
		var vehicleInfo = {};
		vehicleInfo.propertyId = selectedProperty;
		vehicleInfo.zoneId = selectedParkingZone;
		vehicleInfo.licenceNumber = object.licenceNumber;
		vehicleInfo.permitNumber = object.permitNumber;
		vehicleInfo.isParked = 'true';
		vehicleInfo.parkedDate = $scope.date;
		vehicleInfo.parkingAllowedZone = object.allowedParkingZones;
		vehicleInfo.isSynced = 'false';
		vehicleInfo.tenantId = object.tenantId;
		vehicleInfo.tenantName = object.tenantName;
		vehicleInfo.tenantPermitAllowed = object.tenantPermitAllowed;

		console.log(vehicleInfo);

		var data = {
			"userId" : user_id,
			"auth_token" : auth_token,
			"propertyId" : selectedProperty,
			"zoneId" : selectedParkingZone,
			"licenseId" : object.licenceNumber,
			"date" : $scope.date
		};

		if (object.isParked == true) {
			if (parse(localStorage.getItem('isOnline'))) {
				console.log("vehicle parked");

				/*
				 * Check whether the vehicle is already in the db if not insert the values else update the parked status for that date
				 */
				VehiclesParked.getByLicence(object.licenceNumber, selectedProperty, $scope.date).then(function(vehicles) {
					console.log("Vehicle in DB : " + vehicles.length);
					console.log(vehicles);

					if (vehicles.length <= 0) {
						console.log("Insert into DB");
						VehiclesParked.insert(vehicleInfo).then(function() {

						});
					} else {
						console.log("Update the DB : vehicle parked");
						VehiclesParked.update(vehicleInfo.licenceNumber, vehicleInfo.zoneId, vehicleInfo.isParked, $scope.date, false).then(function(vehicles) {
							console.log("Vehicles Updated : " + vehicles.length);
						});
					}
				});

				if (CheckInternet()) {
					var request = Network.post(data, 'addVerifiedVehicle.json');
				} else {
					$ionicLoading.hide();
					$scope.showAlert(MESSAGE.connection_error);
				}
			} else {
				VehiclesParked.getVehicleParkedCount(vehicleInfo.tenantId).then(function(result) {

					console.log(result);
					var parkedCount = result.count;
					if (result.count >= vehicleInfo.tenantPermitAllowed) {
						$scope.showAlert(MESSAGE.tenant_parking_full);
						object.isParked = false;
					} else {
						VehiclesParked.getVehicleParkedZoneCount(vehicleInfo.tenantId, selectedParkingZone).then(function(zoneParkedCount) {
							console.log("Parked in zone : " + selectedParkingZone + 'Count : ' + zoneParkedCount.count);
							VehiclesParked.getReserved(vehicleInfo.tenantId, selectedParkingZone).then(function(result) {
								console.log("Total permit allowed : " + parkedCount);
								console.log("Reserved for parking in selected zone :" + result.reserved);
								if (result.reserved == "") {
									VehiclesParked.getParkedInfo(vehicleInfo.permitNumber, vehicleInfo.isParked, $scope.date).then(function(vehicles) {
										console.log(vehicles);
										if (vehicles.length > 0) {
											$scope.showAlert(MESSAGE.vehicle_already_parked);
											object.isParked = false;

										} else {
											VehiclesParked.getParkingZoneName(selectedParkingZone).then(function(result) {
												console.log(result);
												if (vehicleInfo.parkingAllowedZone.indexOf(result[0].zone_name) <= -1) {
													console.log(vehicleInfo.parkingAllowedZone);
													console.log(result[0].zone_name);
													console.log(vehicleInfo.parkingAllowedZone.indexOf(selectedParkingZone));
													$scope.showAlert(MESSAGE.invalid_parking);
													object.isParked = false;
												} else {
													VehiclesParked.updateParkedInfo(vehicleInfo.zoneId, vehicleInfo.licenceNumber, $scope.date, vehicleInfo.isParked).then(function(result) {
														console.log(result);
														$scope.showAlert(MESSAGE.vehicle_added);
													});
												}
											});
										}
									});
								} else if (zoneParkedCount.count >= result.reserved) {
									$scope.showAlert(MESSAGE.parking_zone_full);
									object.isParked = false;
								} else {
									VehiclesParked.getParkedInfo(vehicleInfo.permitNumber, vehicleInfo.isParked, $scope.date).then(function(vehicles) {
										console.log(vehicles);
										if (vehicles.length > 0) {
											$scope.showAlert(MESSAGE.vehicle_already_parked);
											object.isParked = false;

										} else {
											VehiclesParked.getParkingZoneName(selectedParkingZone).then(function(result) {
												console.log(result);
												if (vehicleInfo.parkingAllowedZone.indexOf(result[0].zone_name) <= -1) {
													console.log(vehicleInfo.parkingAllowedZone);
													console.log(result[0].zone_name);
													console.log(vehicleInfo.parkingAllowedZone.indexOf(selectedParkingZone));
													$scope.showAlert(MESSAGE.invalid_parking);
													object.isParked = false;
												} else {
													VehiclesParked.updateParkedInfo(vehicleInfo.zoneId, vehicleInfo.licenceNumber, $scope.date, vehicleInfo.isParked).then(function(result) {
														console.log(result);
														$scope.showAlert(MESSAGE.vehicle_added);
													});
												}
											});

										}
									});
								}

							});
						});

					}

				});
			}

		} else {
			console.log("Vehicle not parked");
			vehicleInfo.isParked = 'false';
			if (parse(localStorage.getItem('isOnline'))) {

				console.log("Update the DB : vehicle not parked");
				console.log(vehicleInfo);
				VehiclesParked.update(vehicleInfo.licenceNumber, vehicleInfo.zoneId, vehicleInfo.isParked, $scope.date, vehicleInfo.parkingAllowedZone, false).then(function(vehiclesUpdated) {
					console.log("Vehicles Updated : " + vehiclesUpdated);
				});
				if (CheckInternet()) {
					var request = Network.post(data, 'removeVerifiedVehicle.json');
				} else {
					$ionicLoading.hide();
					$scope.showAlert(MESSAGE.connection_error);
				}
			} else {
				VehiclesParked.updateParkedInfo(vehicleInfo.zoneId, vehicleInfo.licenceNumber, $scope.date, vehicleInfo.isParked).then(function(result) {
					console.log(result);
					$scope.showAlert(MESSAGE.vehicle_removed);
				});
			}
		}
		request.success(function(response) {
			console.log(response);
			if (response.error_code === 0) {
				$scope.showAlert(response.message);
				VehiclesParked.updateSyncStatus(object.licenceNumber, $scope.date).then(function(vehicles) {
					console.log("Records updated : " + vehicles.rowsAffected);
				});

			} else if (response.error_code == 3) {

				$scope.showAlert(response.message);
				localStorage.removeItem('user');
				location.href = '#/login';
			} else if (response.error_code == 5) {

				$scope.showAlert(response.message);
				vehicleInfo.isParked = 'false';
				object.isParked = false;
				VehiclesParked.update(vehicleInfo.licenceNumber, vehicleInfo.zoneId, vehicleInfo.isParked, $scope.date, true).then(function(vehiclesUpdated) {
					console.log("Vehicles Updated : " + vehiclesUpdated);

				});

			} else if (response.error_code != 0 && response.error_code != 3) {
				$scope.showAlert(response.message);
			}
		});
		request.error(function(response) {

		});
	};

	$scope.getOfflineData = function() {
		if (offlineSelectedProperty == undefined || offlineSelectedProperty == null || offlineSelectedProperty == "") {
			$scope.showAlert("Please select property");
		} else {
			$scope.showLoading();
			var data = {
				"userId" : user_id,
				"auth_token" : auth_token,
				"propertyId" : offlineSelectedProperty,
				"date" : $scope.date
			};
			console.log(data);
			if (CheckInternet()) {
				var request = Network.post(data, 'getOfflineData.json');
			} else {
				$ionicLoading.hide();
				$scope.showAlert(MESSAGE.connection_error);
			}

			request.success(function(response) {
				$scope.hideLoading();
				console.log(response);
				if (response.error_code == 0) {
					angular.forEach(response.data[0].parkingZone, function(value, key) {
						var parkingZone = {};
						parkingZone.zoneId = value.id;
						parkingZone.zoneName = value.zoneName;
						parkingZone.propertyId = offlineSelectedProperty;

						console.log(parkingZone);

						VehiclesParked.insertParkingZone(parkingZone).then(function(parkingZones) {
							console.log("ParkingZones added");
						});
					});

					angular.forEach(response.data[0].vehicleInfo, function(value, key) {

						var vehicleInfo = {};
						vehicleInfo.propertyId = offlineSelectedProperty;
						vehicleInfo.zoneId = value.parkingZoneId;
						vehicleInfo.licenceNumber = value.licenceNumber;
						vehicleInfo.permitNumber = value.permitNumber;
						vehicleInfo.isParked = value.status;
						vehicleInfo.parkedDate = value.date;
						vehicleInfo.parkingAllowedZone = value.allowedZone;
						vehicleInfo.isSynced = 'true';
						vehicleInfo.tenantId = value.tenantInfo[0].id;
						vehicleInfo.tenantName = value.tenantInfo[0].tenantName;
						vehicleInfo.tenantPermitAllowed = value.tenantInfo[0].numberOfPermits;

						console.log(vehicleInfo);

						var tenantParkingReserved = value.tenantInfo[0].parkingReserved;

						angular.forEach(tenantParkingReserved, function(data, key) {
							var tenantZone = {};
							tenantZone.tenantId = value.tenantInfo[0].id;
							tenantZone.zoneId = data.tenantZoneId;
							tenantZone.reserved = data.tenantParkingReserved;

							var tenantZones = [];
							VehiclesParked.getTenantZoneCount(tenantZone).then(function(tenantZones) {
								console.log("tenantZones : " + tenantZones.count);

								if (tenantZones.count <= 0) {

									VehiclesParked.insertTenantZone(tenantZone).then(function(result) {
										console.log(result);
									});

								} else {
									VehiclesParked.updateTenantZone(tenantZone).then(function(result) {
										console.log(result);
									});
								}
							});

						});

						VehiclesParked.insert(vehicleInfo).then(function(vehicles) {
							console.log('Vehicles Inserted');
						});
					});

					$scope.showAlert(MESSAGE.offline_downloaded);
				} else {
					$scope.showAlert(response.message);
				}

			});
			request.error(function(response) {
				$scope.hideLoading();
				console.log(response);
			});
		}
	};

	$scope.resetResult = function() {
		console.log($scope.user.licenceNum);
		$scope.showHeader = false;
		$scope.records = [];

	};
	$scope.reset = function(user) {
		user.licenceNum = "";
		user.permitNum = "";

	};

	$scope.resetScreen = function() {
		//alert($scope.propertySelected);
		if (angular.isDefined($scope.property)) {
			$scope.property = "";
		}
		if (angular.isDefined($scope.parkingZone)) {
			$scope.parkingZone = "";
		}
		$ionicHistory.clearCache();
		$scope.user.licenceNum = "";
		$scope.user.permitNum = "";
		$scope.showHeader = false;
		$scope.records = [];
		selectedProperty = "";
		selectedParkingZone = "";
		offlineSelectedProperty = "";
	};

	$scope.goHome = function() {
		location.href = "#/app/parksmartInfo";
	};

	$scope.downloadOffline = function() {
		$scope.resetScreen();
		location.href = "#/app/sync";
	};

	$scope.pushOfflineData = function() {
		console.log("getUnSyncedInfo");
		$scope.showLoading();
		var allDataPushed = true;
		VehiclesParked.getUnSyncedInfo().then(function(result) {
			console.log(result);
			if (result.length > 0) {
				angular.forEach(result, function(value, key) {
					if (allDataPushed) {
						var vehicleInfo = {};
						vehicleInfo.licenceId = value.licence_number;
						vehicleInfo.parkedDate = value.parked_date;
						vehicleInfo.isParked = value.is_parked;
						allDataPushed = $scope.pushData(vehicleInfo);
					}
				});
				if (allDataPushed) {

					$scope.showAlert("Pushed offline data to server");
				} else {
					$scope.showAlert("Sync failed, Please try again");
				}
			} else {

				$scope.hideLoading();
				$scope.showAlert("Pushed offline data to server");
			}

		});
	};
	$scope.pushData = function(vehicleInfo) {
		var data = {
			"userId" : user_id,
			"licenseId" : vehicleInfo.licenceId,
			"date" : vehicleInfo.parkedDate,
			"isParked" : vehicleInfo.isParked,
			"auth_token" : auth_token,
		};
		if (CheckInternet()) {
			var request = Network.post(data, 'addOfflineData.json');
		} else {
			$ionicLoading.hide();
			$scope.showAlert(MESSAGE.connection_error);
		}

		request.success(function(response) {
			$scope.hideLoading();
			if (response.error_code == 0) {
				VehiclesParked.updateSyncStatus(vehicleInfo.licenceId, vehicleInfo.parkedDate).then(function(result) {
					console.log(result);
					return true;
				});
			} else {
				$scope.showAlert(response.message);
				return false;
			}
			console.log(response);
		});

		request.error(function(response) {
			$scope.hideLoading();
			console.log(response);
			return false;
		});
	};

	$scope.arrayContains = function(array, str) {
		angular.forEach(array, function(value) {
			if (value == str) {
				return true;
			}
		});
		return false;
	};

}).controller('LoginCtrl', function($scope, $http, $ionicPopup, $ionicLoading, $location, Network, onlineStatus, md5, SECURITY, MESSAGE) {
	// Form data for the login modal
	//$scope.user = {};
	//alert(CheckInternet());
	$scope.showAlert = function(message) {
		var alertPopup = $ionicPopup.alert({
			title : 'Parksmart',
			template : message
		});
		alertPopup.then(function(res) {
			/*
			 * Any thing which need to be performed after clicking OK button on alert
			 */
		});
	};

	// Perform the login action when the user submits the login form
	$scope.doLogin = function(user) {

		if (user.name != "" && user.password != "") {

			$ionicLoading.show({
				content : 'Loading',
				animation : 'fade-in',
				showBackdrop : true,
				maxWidth : 200,
				showDelay : 0
			});

			var data = {
				"username" : user.name,
				"password" : md5.createHash(user.password + SECURITY.salt),
			};
			console.log(data);
			if (onlineStatus.isOnline()) {

				var request = Network.post(data, 'login.json');
				

			} else {

				$ionicLoading.hide();
				$scope.showAlert(MESSAGE.connection_error);
			}

			request.success(function(response) {
				if (response.error_code === 0) {

					user_id = response.data[0].id;
					auth_token = response.data[0].auth_token;
					companyName = response.data[0].company;
					// As required in every request for authentication
					//alert(auth_token);
					localStorage.setItem("user", JSON.stringify(response.data[0]));
					localStorage.setItem("isOnline", 'true');
					
					user.name = undefined;
					user.password = undefined;
					$scope.userForm.$setPristine();
					

					$ionicLoading.hide();
					console.log("Login successful and opening info page");
					location.href = '#/app/parksmartInfo';

				} else if (response.error_code === 2) {
					$ionicLoading.hide();
					$scope.showAlert(response.message);
					user.password = undefined;
					$scope.userForm.$setPristine();
				}else {
					$ionicLoading.hide();
					$scope.showAlert(response.message);
				}

			});

			request.error(function(response) {
				console.log(response);
				$ionicLoading.hide();
			});

		}

	};
	$scope.validateEmail = function(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	};

	$scope.forgotPassword = function() {
		$scope.user = {};
		$scope.showError = false;
		$scope.showInvalid = false;
		// An elaborate, custom popup
		var myPopup = $ionicPopup.show({
			template : '<input type="text" ng-model="user.email"><p ng-if="showError" class="errror">Please enter email id</p><p ng-if="showInvalid" class="errror">Please enter valid email id</p>',
			title : 'Parksmart',
			subTitle : 'Enter registered email id',
			scope : $scope,
			buttons : [{
				text : 'Cancel'
			}, {
				text : '<b>Submit</b>',
				type : 'button-positive',

				onTap : function(e) {
					if (!$scope.user.email) {
						//don't allow the user to close unless he enters wifi password
						$scope.showInvalid = false;
						$scope.showError = true;
						e.preventDefault();
					} else {
						$scope.showError = false;
						if ($scope.validateEmail($scope.user.email)) {
							return $scope.user.email;
						} else {
							$scope.showInvalid = true;
							e.preventDefault();
						}
					}
				}
			}]
		});
		myPopup.then(function(emailId) {

			console.log('Entered EmailId', emailId);
			if (emailId != undefined && emailId != "") {
				var data = {
					"emailId" : emailId
				};
				if (onlineStatus.isOnline()) {

					$ionicLoading.show({
						content : 'Loading',
						animation : 'fade-in',
						showBackdrop : true,
						maxWidth : 200,
						showDelay : 0
					});

					var request = Network.post(data, 'forgetPassword.json');
				} else {
					// $ionicLoading.hide();
					$scope.showAlert(MESSAGE.connection_error);
				}
				request.success(function(response) {
					if (response.result == 'success') {
						$ionicLoading.hide();
						$scope.showAlert(response.message);
					} else {
						$ionicLoading.hide();
						$scope.showAlert(response.message);
					}
				});

				request.error(function(response) {
					$ionicLoading.hide();
				});
			}
		});

	};
}).controller('SplashCtrl', function($scope, $http, $ionicPopup, $ionicLoading, $location, Network, onlineStatus) {
	setTimeout(function() {
		if (localStorage.getItem("user") != undefined && localStorage.getItem("user") != "") {
			location.href = '#/app/parksmartInfo';
		} else {
			location.href = '#/login';
		}

	}, 5000);
});

