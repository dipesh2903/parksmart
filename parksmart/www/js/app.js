// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
db = null;

angular.module('ParkSmart', ['ionic', 'angular-md5', 'ngCordova', 'Parksmart.controllers', 'Parksmart.services']).run(function($ionicPlatform, DB, $cordovaSQLite,$cordovaNetwork) {
	$ionicPlatform.ready(function() {

		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});

	// Initialise the Database
	$ionicPlatform.ready(function() {
		
		
		DB.init();
		

	});

}).run(function($rootScope, $ionicHistory) {

	$rootScope.$ionicGoBack = function() {
		$ionicHistory.goBack();
		$rootScope.notifyIonicGoingBack();
	};

}).config(function($stateProvider, $urlRouterProvider) {
	$stateProvider.state('splash', {
		url : "/splash",
		templateUrl : "templates/splash.html",
		controller : 'SplashCtrl'
	}).state('login', {
		url : "/login",
		templateUrl : "templates/login.html",
		controller : 'LoginCtrl'
	}).state('app', {
		url : "/app",
		abstract : true,
		templateUrl : "templates/menu.html",
		controller : 'AppCtrl'
	}).state('app.parksmartInfo', {
		url : "/parksmartInfo",
		views : {
			'menuContent' : {
				templateUrl : "templates/parkSmartInfo.html"
			}

		}
	}).state('app.sync', {
		url : "/sync",
		views : {
			'menuContent' : {
				templateUrl : "templates/sync.html"
			}
		}
	});

	if (localStorage.getItem("user") != undefined && localStorage.getItem("user") != "") {
		//location.href = '#/app/parksmartInfo';
		$urlRouterProvider.otherwise('/app/parksmartInfo');
	} else {
		//location.href = '#/login';
		$urlRouterProvider.otherwise('/login');
	}

	// if none of the above states are matched, use this as the fallback
	//$urlRouterProvider.otherwise('/splash');
});
